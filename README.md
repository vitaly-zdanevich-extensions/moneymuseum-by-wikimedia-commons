![logo](/logo.webp)

![screenshot](/screenshot.png)

![overview](/overview.webm)

Browser extension for https://moneymuseum.by that shows links to Wikimedia Commons.

Also you can use this as a userscript with [Violentmonkey](https://violentmonkey.github.io/) or another script manager.
